


$(document).ready(function() {


	$(window).scroll(function() {

		var wScroll = $(this).scrollTop();

		$('.logo').addClass('animated_logo');
		setTimeout(function() {
			$('.header__top_part').addClass('animated_header__top_part');
		}, 600);


	});





	//owl carousel
	function owlCarWidth(carousel) {

		var
		totalWidth = 0;

		carousel.find('.owl-item').each(function() {
			totalWidth += $(this).width();
			
		});

		carousel.find('.owl-stage').width(totalWidth+2000);

	}
	owlCarWidth( $('.s_films__carousel') );

	$('.s_films__carousel').on('refreshed.owl.carousel', function(event) {
		owlCarWidth( $('.s_films__carousel') );
	});

	$(window).trigger('resize');



	function heroesCarWidth(carousel) {

		var
		totalWidth = 0;

		carousel.find('.owl-item').each(function() {
			totalWidth += $(this).width();
			
		});

		carousel.find('.owl-stage').width(totalWidth+2000);

	}
	heroesCarWidth( $('.heroes_carousel') );

	$('.heroes_carousel').on('refreshed.owl.carousel', function(event) {
		heroesCarWidth( $('.heroes_carousel') );
	});

	$(window).trigger('resize');

	$('.header__carousel').owlCarousel({

		loop: true,
		nav: true,
		navText: '',
		items: 1,
		dots: false,
	});


	$('.s_about__carousel').owlCarousel({
		pagination: true,
		loop: true,
		nav: true,
		navText: '',
		items: 1,
		dots: false,

	});


	$('.heroes_carousel').owlCarousel({
		pagination: true,
		loop: true,
		nav: true,
		center: true,
		navText: '',
		dots: false,
		responsive: {
			
			0: {items: 2,
				margin: 20,
				center: true,
				autoWidth: true,},
				768:  { items: 1,
					margin: 0,

					autoWidth: false,},
					1366: {items: 2,
						margin: 80,

						autoWidth: true,},
					},

				});

	


	$('.s_films__carousel').owlCarousel({
		loop: true,
		nav: true,
		navText: '',
		dots: false,
		responsive: {

			0:  { items: 1,
				center: false,
				margin: 0, 
				autoWidth: false,},

				640:  { items: 2,
					center: true,
					margin: 20, 
					autoWidth: true,},

					768: { items: 2,
						center: true,
						margin: 30,
						autoWidth: true,},

						1024 : { items: 1,
							center: false,
							margin: 0, 
							autoWidth: false,},
						},
					});






	//Всплывающее меню
	$(".number").click(function() {
		if($(this).parent().children("ul").is (":visible")) {
			$(this).parent().children("ul").slideUp();
		} else {
			$("body .m_item > ul").hide();
			$(".m_item").removeClass("active");
			$(this).parent().addClass("active");
			$(this).parent().children("ul").slideToggle();
		}
	});


	//Magnific Popup
	$(".popup_content").magnificPopup({tipe:"inline", midClick: true, closeOnBgClick: true});

	$(document).on('click', '#self_link', function() {
		$.magnificPopup.close();
		$.magnificPopup.open({
			items: {
				src: '#form_about_self'
			}
		});

	});



	$('select').styler({
		fileBrowse: '',
		singleSelectzIndex: '9999',
		selectSearchLimit: '4',
	});

	$(document).on('click', '#friend_link', function() {
		$.magnificPopup.close();
		$.magnificPopup.open({
			items: {
				src: '#form_about_friend'
			}
		});

	});



	//	Phone Mask
	$(function() {
		jQuery.fn.exists = function() {
			return jQuery(this).length;
		}

		if($('#user_phone').exists()){

			$('#user_phone').each(function(){
				$(this).mask("+7(999) 999-99-99");
			});

		}

		if($('form').exists()){

			var form = $('form'),
			btn = form.find('.btn_submit');

			form.find('.rfield').addClass('empty_field');

			setInterval(function(){

				if($('#user_phone').exists()){
					var pmc = $('#user_phone');
					if ( (pmc.val().indexOf("_") != -1) || pmc.val() == '' ) {
						pmc.addClass('empty_field');
					} else {
						pmc.removeClass('empty_field');
					}
				}

				var sizeEmpty = form.find('.empty_field').size();

				if(sizeEmpty > 0){
					if(btn.hasClass('disabled')){
						return false
					} else {
						btn.addClass('disabled')
					}
				} else {
					btn.removeClass('disabled')
				}

			},200);



		}


	});


	
	






  //Временные переходы между модальными окнами
  $(document).on('click', '.modal_news #news_submit', function() {
  	$.magnificPopup.close();
  	$.magnificPopup.open({
  		items: {
  			src: '#m_news_2'
  		}
  	});

  });

  $(document).on('click', '.modal_character_historiy_vote__button', function() {
  	$('.modal_character_historiy_vote__button').addClass('hidden');
  	$('.modal_character_historiy_vote__thank').removeClass('hidden');

  });
  

  $(document).on('click', '.social_authorization_button', function() {
  	$.magnificPopup.close();
  	$.magnificPopup.open({
  		items: {
  			src: '#social_authorization_done'
  		}
  	});

  });

  $(document).on('click', '#vote__button', function() {
  	$.magnificPopup.close();
  	$.magnificPopup.open({
  		items: {
  			src: '#modal_vote_confirmation'
  		}
  	});

  });

 



});


